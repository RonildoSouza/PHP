drop database if exists financeiro;
create database financeiro;
use financeiro;

create table clientes(
id_cliente int primary key not null auto_increment,
nome_cliente varchar(100),
fone_cliente varchar(12),
email_cliente varchar(100),
endereco_cliente varchar(100)
);

create table fornecedores(
id_fornecedor int primary key not null auto_increment,
nome_fornecedor varchar(100),
fone_fornecedor varchar(12),
email_fornecedor varchar(100),
cnpj_fornecedor varchar(20),
endereco_fornecedor varchar(100)
);

create table contaspagar(
id_contaspagar int primary key not null auto_increment,
documento_contaspagar varchar(30),
valor_contaspagar double,
fornecedor_contaspagar int not null,
status_contaspagar enum('N','S'),
vencimento_contaspagar date,
foreign key(fornecedor_contaspagar) references fornecedores (id_fornecedor)
);

create table contasreceber(
id_contasreceber int primary key not null auto_increment,
documento_contasreceber varchar(30),
valor_contasreceber double,
cliente_contasreceber int not null,
status_contasreceber enum('N','S'),
vencimento_contasreceber date,
foreign key(cliente_contasreceber) references clientes (id_cliente)
);

