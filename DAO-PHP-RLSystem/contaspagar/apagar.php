<?php
// By RL System - www.rlsystem.com.br
// Incluo os arquivos necess�rios
include "../Conexao.php";
include "../Classes/ContasPagar.php";
include "../DAO/ContasPagarDAO.php";
	
// Verifico se existe a query string ID.
if (isset($_GET['ID']))
{
	// Instancio a Classe Contas Pagar
	$CP = new ContasPagar();
	// Seto o ID do registo que ser� recuperado
	$CP->setId_contaspagar($_GET["ID"]);
	
	// Instancio a Classe ContasPagarDAO
	$CPDAO = new ContasPagarDAO(); 
	
	// Chamo o m�todo para exclus�o, caso retorne verdaderio, ou seja conseguiu deletar o registro, ele entra no if e exibe o resultado de sucesso.
	if ($CPDAO->DeleteContasPagar($CP))
	{
		echo "<script>alert('Conta a pagar, deletada com succeso!');</script>";
	}
}
?>