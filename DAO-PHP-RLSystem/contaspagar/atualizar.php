<style type="text/css">
@import url(../padrao.css);
</style>
<?php
// By RL System - www.rlsystem.com.br
// Incluo os arquivos necess�rios
include "../Conexao.php";
include "../Classes/ContasPagar.php";
include "../DAO/ContasPagarDAO.php";

	// Instancio a Classe Contas Pagar
	$CP = new ContasPagar();
	
	// Seto o ID do registo que ser� recuperado
	$CP->setId_contaspagar($_GET["ID"]); 
	
	// Instancio a Classe ContasPagarDAO
	$CPDAO = new ContasPagarDAO();
	
	//
	$CP->setFornecedor_contaspagar($CP->getFornecedor_contaspagar());
	
// Verifico se existe a query string atualizar.
if (isset($_GET['atualizar']))
{
	// Seto os valores, que est�o sendo recuperado pelas vari�veis globais $_POST
	$CP->setDocumento_contaspagar($_POST['txtDocumento']);
	$CP->setValor_contaspagar($_POST['txtValor']);
	$CP->setFornecedor_contaspagar($_POST['cbFornecedor']);
	$CP->setVencimento_contaspagar($_POST['txtData']);
	$CP->setStatus_contaspagar($_POST['txtStatus']);
	
	// Chamo o m�todo para atualiza��o, caso retorne verdaderio, ou seja conseguiu atualizar o registro, ele entra no if e exibe o resultado de sucesso.
	if ($CPDAO->UpdateContasPagar($CP))
	{
		echo "<script>alert('Conta a pagar, atualizada com succeso!');</script>";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="pt-br" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Cadastro de Contas a Pagar</title>
</head>

<body>

<?php 
			// M�todo para trazer as informa��es, passando como parametro um objeto do tipo ContasPagar
			$CPDAO->ShowContasPagar($CP);
?>
<h1>ATUALIZAR CONTAS A PAGAR</h1>
<hr/>
<?php 
/*
 * Veja que estamos informando na action do formulario o nome da p�gina, mais uma query string atualizar
 * E outra informando o ID da p�gina, que � recuperado pela query string.
 * Com isso quando subtermos as informa��es, como voc� pode ver acima do c�digo ele faz as devidas verifica��es,
 * e tamb�m sabe que registros estaremos editando.
*/
?>
<form action="atualizar.php?atualizar&ID=<?=$_GET["ID"];?>" method="post">
	<table style="width: 100%" class="ms-classic3-main">
		<!-- fpstyle: 6,011111100 -->
		<tr>
			<td style="width: 136px" class="ms-classic3-tl">Documento:</td>
			<td class="ms-classic3-top"><input name="txtDocumento" value="<?=$CP->getDocumento_contaspagar();?>" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Valor:</td>
			<td class="ms-classic3-even"><input name="txtValor" value="<?=$CP->getValor_contaspagar();?>" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Fornecedor:</td>
			<td class="ms-classic3-even">
			<select name="cbFornecedor">
			<?php
			/*
			 * Realizo um foreach no m�todo ShowFornecedores, que me retorna as op��es de uma select list.
			 * */
			foreach($CPDAO->ShowFornecedores($CP) as $exibir)
			{
				echo $exibir;
			}
			?>
			</select>
			</td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Data de Vencimento:</td>
			<td class="ms-classic3-even"><input name="txtData" value="<?=$CP->getVencimento_contaspagar();?>" style="width: 127px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Status:</td>
			<td class="ms-classic3-even">
			<input name="txtStatus" value="<?=$CP->getStatus_contaspagar();?>" style="width: 127px" type="text" /> (S = Postivo(Pago), N = Negativo)</td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">&nbsp;</td>
			<td class="ms-classic3-even"><input name="btCadastrar" type="submit" value="Atualizar" /></td>
		</tr>
	</table>
</form>

</body>

</html>
