<style type="text/css">
@import url(../padrao.css);
</style>
<?php
// By RL System - www.rlsystem.com.br
// Incluo os arquivos necess�rio
include "../Conexao.php";
include "../Classes/ContasPagar.php";
include "../DAO/ContasPagarDAO.php";

// Verifico se existe a query string cadastro.
if (isset($_GET['cadastro']))
{
	// Instancio a Classe Contas Pagar
	$CP = new ContasPagar();
	
	// Seto os valores, que est�o sendo recuperado pelas vari�veis globais $_POST
	$CP->setDocumento_contaspagar($_POST['txtDocumento']);
	$CP->setValor_contaspagar($_POST['txtValor']);
	$CP->setFornecedor_contaspagar($_POST['cbFornecedor']);
	$CP->setVencimento_contaspagar($_POST['txtData']);
	$CP->setStatus_contaspagar("N");
	
	// Instancio a Classe ContasPagarDAO
	$CPDAO = new ContasPagarDAO();
	
	// Caso retorne verdaderio, ou seja conseguiu cadastrar ele entra no if e exibe o resultado e j� redireciona;
	if ($CPDAO->InsertContasPagar($CP))
	{
		echo "<script>alert('Conta a pagar, cadastrada com succeso!');</script>";
		echo "<script>window.location = 'listar.php';</script>";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="pt-br" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Cadastro de Contas a Pagar</title>
</head>

<body>

<h1>CADASTRO CONTAS A PAGAR</h1>
<hr/>
<form action="?cadastro" method="post">
	<table style="width: 100%" class="ms-classic3-main">
		<!-- fpstyle: 6,011111100 -->
		<tr>
			<td style="width: 136px" class="ms-classic3-tl">Documento:</td>
			<td class="ms-classic3-top"><input name="txtDocumento" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Valor:</td>
			<td class="ms-classic3-even"><input name="txtValor" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Fornecedor:</td>
			<td class="ms-classic3-even">
			<select name="cbFornecedor">
			<?php
			/* Intancio a classe ContasPagar, j� que meu m�todo ShowFornecedores da classe ContasPagarDAO,
			 * Pede um objeto do tipo "ContasPagar".
			*/
			$CP = new ContasPagar();
			// Intancio a classe ContasPagarDAO 
			$CPDAO = new ContasPagarDAO();
			/*
			 * Fa�o um foreach no m�todo ShowFornecedores, que me retorna as op��es de uma select list.
			 * */
			foreach($CPDAO->ShowFornecedores($CP) as $exibir)
			{
				echo $exibir;
			}
			?>
			</select>
			</td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Data de Vencimento:</td>
			<td class="ms-classic3-even"><input name="txtData" style="width: 127px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">&nbsp;</td>
			<td class="ms-classic3-even"><input name="btCadastrar" type="submit" value="Cadastrar" /></td>
		</tr>
	</table>
</form>

</body>

</html>
