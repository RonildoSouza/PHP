<?php
// Incluo os arquivos necess�rios
include "../Conexao.php";
include "../Classes/ContasReceber.php";
include "../DAO/ContasReceberDAO.php";
	
// Verifico se existe a query string ID.
if (isset($_GET['ID']))
{
	// Instancio a Classe Contas Receber
	$CP = new ContasReceber();
	// Seto o ID do registo que ser� recuperado
	$CP->setId_contasreceber($_GET["ID"]);
	
	// Instancio a Classe ContasReceberDAO
	$CPDAO = new ContasReceberDAO(); 
	
	// Chamo o m�todo para exclus�o, caso retorne verdaderio, ou seja conseguiu deletar o registro, ele entra no if e exibe o resultado de sucesso.
	if ($CPDAO->DeleteContasReceber($CP))
	{
		echo "<script>alert('Conta a receber, deletada com succeso!');</script>";
	}
}
?>