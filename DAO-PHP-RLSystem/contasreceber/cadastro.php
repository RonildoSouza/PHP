<style type="text/css">
@import url(../padrao.css);
</style>
<?php
// Incluo os arquivos necess�rio
include "../Conexao.php";
include "../Classes/ContasReceber.php";
include "../DAO/ContasReceberDAO.php";

// Verifico se existe a query string cadastro.
if (isset($_GET['cadastro']))
{
	// Instancio a Classe Contas Receber
	$CP = new ContasReceber();
	
	// Seto os valores, que est�o sendo recuperado pelas vari�veis globais $_POST
	$CP->setDocumento_contasreceber($_POST['txtDocumento']);
	$CP->setValor_contasreceber($_POST['txtValor']);
	$CP->setCliente_contasreceber($_POST['cbCliente']);
	$CP->setVencimento_contasreceber($_POST['txtData']);
	$CP->setStatus_contasreceber("N");
	
	// Instancio a Classe ContasReceberDAO
	$CPDAO = new ContasReceberDAO();
	
	// Caso retorne verdaderio, ou seja conseguiu cadastrar ele entra no if e exibe o resultado e j� redireciona;
	if ($CPDAO->InsertContasReceber($CP))
	{
		echo "<script>alert('Conta a receber, cadastrada com succeso!');</script>";
		echo "<script>window.location = 'listar.php';</script>";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="pt-br" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Cadastro de Contas a Receber</title>
</head>

<body>

<h1>CADASTRO CONTAS A RECEBER</h1>
<hr/>
<form action="?cadastro" method="post">
	<table style="width: 100%" class="ms-classic3-main">
		<!-- fpstyle: 6,011111100 -->
		<tr>
			<td style="width: 136px" class="ms-classic3-tl">Documento:</td>
			<td class="ms-classic3-top"><input name="txtDocumento" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Valor:</td>
			<td class="ms-classic3-even"><input name="txtValor" style="width: 292px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Clientes:</td>
			<td class="ms-classic3-even">
			<select name="cbCliente">
			<?php
			/* Intancio a classe ContasReceber, j� que meu m�todo ShowClientes da classe ContasReceberDAO,
			 * Pede um objeto do tipo "ContasReceber".
			*/
			$CP = new ContasReceber();
			// Intancio a classe ContasReceberDAO 
			$CPDAO = new ContasReceberDAO();
			/*
			 * Fa�o um foreach no m�todo ShowClientes, que me retorna as op��es de uma select list.
			 * */
			foreach($CPDAO->ShowClientes($CP) as $exibir)
			{
				echo $exibir;
			}
			?>
			</select>
			</td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">Data de Vencimento:</td>
			<td class="ms-classic3-even"><input name="txtData" style="width: 127px" type="text" /></td>
		</tr>
		<tr>
			<td style="width: 136px" class="ms-classic3-left">&nbsp;</td>
			<td class="ms-classic3-even"><input name="btCadastrar" type="submit" value="Cadastrar" /></td>
		</tr>
	</table>
</form>

</body>

</html>
