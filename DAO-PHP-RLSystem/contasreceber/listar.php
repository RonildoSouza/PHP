<?php 
// Incluo os arquivos necess�rio
include "../Conexao.php";
include "../Classes/ContasReceber.php";
include "../DAO/ContasReceberDAO.php";
?>

<head>
<style type="text/css">
@import url(../padrao.css);
</style>
</head>

<h1>LISTA DE CONTAS A RECEBER</h1>
<hr/>
<table class="ms-classic3-main" style="width: 77%">
	<!-- fpstyle: 6,011111100 -->
	<tr>
		<td class="ms-classic3-tl" style="width: 209px">Documento:</td>
		<td class="ms-classic3-top" style="width: 165px">Valor:</td>
		<td class="ms-classic3-top" style="width: 160px">Status</td>
		<td class="ms-classic3-top" style="width: 66px">Editar</td>
	</tr>
	<?php
		// Inst�ncio a classe ContasReceber
		$CP = new ContasReceber();
		
		// Inst�ncio a classe ContasReceberDAO
		$CPDAO = new ContasReceberDAO();
		
		// Atribuo a query que foi executada a minha v�riavel $query
		$query = $CPDAO->ShowContasReceber($CP);
		
		// Fa�o um loop, utilizando o m�todo fetch_array() que estar� exibindo os registros.
		while($reg = $query->fetch_array())
		{
	?>
	<tr>
		<td class="ms-classic3-left" style="width: 209px"><?=$reg["documento_contasreceber"];?></td>
		<td class="ms-classic3-even" style="width: 165px"><?=$reg["valor_contasreceber"];?></td>
		<td class="ms-classic3-even" style="width: 160px"><?=$reg["status_contasreceber"];?></td>
		<td class="ms-classic3-even" style="width: 66px">
		<center><a href="atualizar.php?ID=<?=$reg["id_contasreceber"];?>">
		<img alt="" height="16" src="../imagens/editar.png" width="16" class="style1" border="0"></a>
		<a href="apagar.php?ID=<?=$reg["id_contasreceber"];?>">
		<img alt="" height="16" src="../imagens/apagar.png" width="16" class="style1" border="0"></a></center></td>
	</tr>
	<?php 
		}
	?>
</table>