<?php 
$pasta = str_replace('controller', '', __DIR__);
require $pasta . '/model/cliente.php'; 

class ClienteController 
{ 
	function __construct()
	{
		if(isset($_POST['acao']))
		{
			$acao = $_POST['acao'];

			switch ($acao) {
			case 'deletar':

				$this->deletar($_POST['codigo']);
				
				break;
			
			default:
				echo "Variavel $acao vazia!";
				break;
			}
		}		
	}

	public function listar() 
	{ 
		$cliente = new Cliente(); 
		$clientes = $cliente->listAll();

		$_REQUEST['resultadoSelect'] = $clientes;
		require_once 'view/cliente_view.php'; 
	} 

	public function deletar($codigo) 
	{ 
		$cliente = new Cliente(); 
		$cliente->setId($codigo);
		$cliente->remove();
		
		header('refresh:1;url=../index.php');
	} 
} 

new ClienteController();

?>