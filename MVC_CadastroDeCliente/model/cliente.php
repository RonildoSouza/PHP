<?php
$pasta = str_replace('model', '', __DIR__);
require $pasta . '/conexao.php';

class Cliente
{
	private $id;
	private $nome;
	
	//function __construct($id, $nome)
	//{
	//	$this->setId($id);
	//	$this->setNome($nome)
	//}

//PROPRIEDADES GET, SET
	public function getId()
	{
	    return $this->id;
	}
	
	public function setId($id)
	{
	    $this->id = $id;
	    return $this;
	}

	public function getNome()
	{
	    return $this->nome;
	}

	public function setNome($nome)
	{
	    $this->nome = $nome;
	    return $this;
	}
// FIM PROPRIEDADES

// METODOS

	public function save() 
	{ // logica para salvar cliente no banco 
		
	} 

	public function update() 
	{ // logica para atualizar cliente no banco 

	} 

	public function remove() 
	{ // logica para remover cliente do banco 
		global $con;	

		$sql = $con->prepare("DELETE FROM cliente WHERE codigo = ?");		
		$sql->bind_param("i", $P1);
		$P1 = $this->getId();
		$sql->execute();
		
		//$sql = "DELETE FROM cliente WHERE codigo = " . $this->getId();	
		//$con->query($sql);
	} 

	public function listAll() 
	{ // logica para listar toodos os clientes do banco 
		global $con;	

		$query = "SELECT * FROM cliente";	

		$sql = $con->query($query);

		return $sql;
	}
}