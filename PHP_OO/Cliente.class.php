<?php

	include_once 'ConexaoBancoDeDados.class.php';
	
	class Cliente
	{
		private $codigo;
		private $nome;
		
		// PROPRIEDADES
		public function SetCodigo($value) 
		{
			if(is_numeric($value) and ($value > 0)) 
			{
				$this->codigo = $value;							
			}
		}
		
		public function GetCodigo() 
		{
			return $this->codigo;
		}
		
		public function SetNome($value) 
		{
			$this->nome = $value;
		}
		
		public function GetNome()
		{
			return $this->nome;
		}
		
		// CONSTRUTOR
		function __construct(/*$pCodigo,*/ $pNome)
		{
			//$this->SetCodigo($pCodigo);
			$this->SetNome($pNome);
		}
		
		// METODO
		function Inserir() 
		{
			$retornoPDO = ConexaoBancoDeDados::ConectaNoBancoDeDados();

			$sql = "INSERT INTO cliente(nome) VALUES(:nome)";
			$stmt = $retornoPDO->prepare( $sql );			
			//$stmt->bindParam( ':codigo', $this->codigo , PDO::PARAM_INT );
			$stmt->bindParam( ':nome', $this->nome );
			$stmt->execute();
		}		
	}
?>