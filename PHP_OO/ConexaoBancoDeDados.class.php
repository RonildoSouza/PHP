<?php

include_once 'config.php';

class ConexaoBancoDeDados
{
	public static function ConectaNoBancoDeDados()
	{
		try 
		{
			$PDO = new PDO('mysql:host=' . HOST .';dbname=' . DB_NAME, USER, PASSWORD);
			return $PDO;
		} 
		catch (PDOException $e) 
		{
			 echo 'Erro ao conectar com o MySQL: ' . $e->getMessage();
		}
	}	
}
	
?>